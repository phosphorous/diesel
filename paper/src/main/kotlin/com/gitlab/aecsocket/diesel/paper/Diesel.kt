package com.gitlab.aecsocket.diesel.paper

import com.gitlab.aecsocket.diesel.paper.component.VehicleEngine
import com.gitlab.aecsocket.diesel.paper.component.VehicleSystem
import com.gitlab.aecsocket.alexandria.paper.AlexandriaAPI
import com.gitlab.aecsocket.alexandria.paper.BasePlugin
import com.gitlab.aecsocket.alexandria.paper.PluginManifest
import com.gitlab.aecsocket.sokol.paper.SokolAPI
import net.kyori.adventure.text.format.TextColor
import org.spongepowered.configurate.objectmapping.ConfigSerializable

private const val BSTATS_ID = -1

private lateinit var instance: Diesel
val DieselAPI get() = instance

class Diesel : BasePlugin(PluginManifest("diesel",
    accentColor = TextColor.color(0xd8d443),
    langPaths = listOf(
        "lang/default_en-US.conf"
    ),
    savedPaths = listOf(
        "settings.conf"
    )
)) {
    @ConfigSerializable
    data class Settings(
        val enableBstats: Boolean = true
    )

    init {
        instance = this
    }

    lateinit var settings: Settings private set

    override fun onEnable() {
        super.onEnable()
        DieselCommand(this)
        AlexandriaAPI.registerConsumer(this,
            onInit = {
                 serializers
            },
            onLoad = {
                addDefaultI18N()
            }
        )
        SokolAPI.registerConsumer(
            onInit = {
                engine
            }
        )
    }
}
