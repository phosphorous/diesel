package com.gitlab.aecsocket.diesel.paper

import com.gitlab.aecsocket.alexandria.paper.BaseCommand

internal class DieselCommand(
    override val plugin: Diesel
) : BaseCommand(plugin) {
    init {

    }
}
