<div align="center">

<h1> <a href="https://phosphorous.gitlab.io/diesel">
<img src="icon.svg" height="64"> Diesel
</a> </h1>

[![Latest version](https://img.shields.io/maven-metadata/v?metadataUrl=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F39618617%2Fpackages%2Fmaven%2Fcom%2Fgitlab%2Faecsocket%2Fdiesel%2Fdiesel-core%2Fmaven-metadata.xml)](https://gitlab.com/phosphorous/diesel/-/packages/8022977)
[![Pipeline status](https://img.shields.io/gitlab/pipeline-status/phosphorous/diesel?branch=main)](https://gitlab.com/phosphorous/diesel/-/pipelines/latest)

</div>

Vehicle framework

### [Quickstart and documentation](https://phosphorous.gitlab.io/diesel)
